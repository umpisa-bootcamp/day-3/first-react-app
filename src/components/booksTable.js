import React from 'react'

const BooksTable = ({
    books,
    // fetchBooks,
    onClickRow,
    deleteBook,
    onClickEdit
}) => {
    return (
        <div>
            {/* <button onClick={fetchBooks}>Get Books</button> */}
            <table>
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                </tr>
                </thead>
                <tbody>
                    {books.map((book, index) =>
                        <tr
                            key={`tr_books_${index}`}
                            onClick={() => onClickRow(book)}
                        >
                            <td>{book.title}</td>
                            <td>{book.description}</td>
                            <td>
                                <button onClick={() => onClickEdit(book)}>Edit</button>
                            </td>
                            <td>
                                <button onClick={() => deleteBook(book.id)}>Delete</button>
                            </td>
                        </tr>
                    )}
                </tbody>
            </table>
        </div>
    )
}

export default BooksTable;