import React, {useState, useEffect} from 'react';

const BooksCreateForm = ({
    onCreate,
    book,
    onEdit,
    onCancel,
}) => {
    const [state, setState] = useState({
        title: book ? book.title : '',
        description: book ? book.description : '',
    })

    useEffect(() => {
        setState({
            ...state,
            title: book.title,
            description: book.description
        })
    }, [book])

    const onChangeField = (event, field) => {
        setState({
            ...state,
            [field]: event.target.value
        })
    }

    const onSubmit = (data) => {
        if (Object.keys(book).length > 0) {
            onEdit(data, book.id)
        } else {
            onCreate(data)
        }
    }

    return (
        <div>
            <p>Title</p>
            <input
                type="text"
                onChange={e => onChangeField(e, 'title')}
                defaultValue={state.title}
            />
            <p>Description</p>
            <input
                type="text"
                onChange={e => onChangeField(e, 'description')}
                defaultValue={state.description}
            />
            <br />
            <p>
                <button onClick={() => onCancel()}>Cancel</button>
                &nbsp;
                <button onClick={() => onSubmit(state)}>Save</button>
            </p>
        </div>
    )
}

export default BooksCreateForm;