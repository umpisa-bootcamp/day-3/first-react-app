import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
} from 'react-router-dom'
import { createBrowserHistory } from "history";
import './App.css';
import HomePage from './containers/HomePage';
import CreateBookPage from './containers/CreateBook';

function App() {
  const history = createBrowserHistory();
  return (
    <Router history={history}>
      <div>
        <nav>
          <ul>
            <li>
              <Link to='/'>Home Page</Link>
            </li>
            <li>
              <Link to='/create'>Create Book</Link>
            </li>
          </ul>
        </nav>

        <Switch>
          <Route path='/' exact component={HomePage} />
          <Route path='/create' exact component={CreateBookPage} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
