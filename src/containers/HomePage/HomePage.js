import React, {useState, useEffect} from 'react';
import {
    useHistory
} from 'react-router-dom'
import BooksTable from '../../components/booksTable';

function HomePage() {
  const history = useHistory()
  const [state, setState] = useState({
    name: 'Jules',
    index: true,
    books: [],
    book: {},
  })

  const fetchBooks = () => {
    fetch('http://localhost:3001/v1/books')
      .then(res => res.json())
      .then(res => setState({...state, books: res.data}))
  }

  const deleteBook = (id) => {
    fetch(`http://localhost:3001/v1/books/${id}`, {
      method: 'DELETE',
    }).then(() => fetchBooks())
  }

  useEffect(() => {
    fetchBooks();
  }, [])

  const onClickRow = (book) => {
    setState({
      ...state,
      book,
    });
  }

  const onClickEdit = (book) => {
    history.push({
        pathname: '/create',
        state: {
            book
        }
    })
  }

  const onClickCreate = () => {
    history.push({
        pathname: '/create',
        state: {
            book: {}
        }
    })
  }

  return (
    <div className="App">
      <header className="App-header">
        <button onClick={onClickCreate}>Create Book</button>
        <br />
        <br />
        <BooksTable
          onClickRow={onClickRow}
          books={state.books}
          fetchBooks={fetchBooks}
          onClickEdit={onClickEdit}
          deleteBook={deleteBook} />
      </header>
    </div>
  );
}

export default HomePage;
