import React, { useState } from 'react';
import BookCreateTable from '../../components/booksCreateForm';
import {
  useHistory
} from 'react-router-dom';

function CreateBook() {
  const history = useHistory()
  const historyState = history.location.state
  const [state, setState] = useState({
    book: historyState ? historyState.book : {},
  })

  const createBook = (data) => {
    fetch('http://localhost:3001/v1/books', {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify(data)
    }).then(() => history.push('/home'))
  }

  const editBook = (data, id) => {
    fetch(`http://localhost:3001/v1/books/${id}`, {
      method: 'PUT',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify({
        title: data.title,
        description: data.description,
      })
    }).then(() => history.push('/home'))
  }

  const onCancel = () => {
    history.push('/')
  }

  return (
    <div style={{textAlign: 'center'}}>
      <BookCreateTable
        book={state.book}
        onCreate={createBook}
        onCancel={onCancel}
        onEdit={editBook} />
    </div>
  );
}

export default CreateBook;
